const jwt = require("jsonwebtoken");
const { User } = require("../../models/userModel");

exports.Register = async (req, res) => {
  const { email, password, firstName, lastName } = req.body;

  try {
    const user = await User.create({
      email,
      password,
      firstName,
      lastName,
    });

    const token = jwt.sign({ ...user }, process.env.JSON_SECRET);
    res.send(token);
  } catch (er) {
    res.send(er);
    console.log(er.data);
  }
};

exports.Login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.find({ email: email });
    user.length === 0 && res.send("There is no user with this email.");

    const token = jwt.sign({ ...user }, process.env.JSON_SECRET);
    user[0].password === password
      ? res.send(token)
      : res.send("Incorrect password");
  } catch (er) {
    res.send(er);
  }
};

exports.CheckUser = async (req, res) => {
  const { authorization } = req.headers;

  jwt.verify(authorization, process.env.JSON_SECRET, async (er, dec) => {
    const [{ email }] = await User.find({ email: dec[0].email });
    res.send(email);
  });
};

exports.Logout = ({ body }, res) => {
  jwt.Logout(body.token).then((res) => res.send(res));
};
