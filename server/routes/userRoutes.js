const userRoutes = require("express").Router();
const jwt = require("jsonwebtoken");
const {
  Register,
  Login,
  Logout,
  CheckUser,
} = require("../controller/UserController/UserController");

userRoutes.route("/login").post(Login);
userRoutes.route("/register").post(Register);
userRoutes.route("/checkuser").get(CheckUser);
userRoutes.route("/logout").delete(Logout);

module.exports = userRoutes;
