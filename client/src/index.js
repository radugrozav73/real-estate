import ReactDOM from "react-dom";
import App from "./Routes/App";
import { ThemeProvider } from "styled-components";
import { theme } from "./components/styled-components/theme";
ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>,
  document.getElementById("root")
);
