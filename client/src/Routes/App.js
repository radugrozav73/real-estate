import Home from "../components/Home";
import { Routes, Route } from "react-router-dom";
import Login from "../components/Authentication/Login";
import Register from "../components/Authentication/Register";
import ControlPannel from "../components/ControlPannel/ControlPannel";
import Navbar from "../components/Navbar/Navbar";
import { BrowserRouter as Router } from "react-router-dom";
import CheckAuth from "../middlewares/CheckAuth";
import { AuthProvider } from "../components/context";

function App() {
  return (
    <AuthProvider>
      <Router>
        <Navbar />
        <Routes>
          <Route
            path="/admin"
            exact
            element={
              <CheckAuth>
                <ControlPannel />
              </CheckAuth>
            }
          />
          <Route path="/login" exact element={<Login />} />
          <Route path="/register" exact element={<Register />} />
          <Route path="/" exact element={<Home />} />
        </Routes>
      </Router>
    </AuthProvider>
  );
}

export default App;
