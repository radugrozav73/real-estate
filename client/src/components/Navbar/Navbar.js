import React from "react";
import styled from "styled-components";
import { Link, useLocation } from "react-router-dom";

const Nav = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  background-color: #dddddd;
`;

const LinkWrapper = styled.div`
  width: 600px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  a {
    text-decoration: none;
    color: black;
  }
`;

function Navbar() {
  const location = useLocation();
  const pathNames = ["/login", "/register", "/admin"];

  return pathNames.includes(location.pathname) ? null : (
    <Nav>
      <LinkWrapper>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/careears">Careears</Link>
        <Link to="/contact">Contact</Link>
      </LinkWrapper>
    </Nav>
  );
}

export default Navbar;
