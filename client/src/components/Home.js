import { useEffect, useState } from "react";
import axios from "axios";

function Home() {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [token, setToken] = useState("");
  useEffect(() => {
    console.log(password, email);
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(password, email);
    axios
      .post("http://localhost:3000/login", { password, email })
      .then((res) => {
        console.log(res);
        setToken(res.data);
      });
  };

  const handleLogout = () => {
    axios
      .delete("http://localhost:3000/logout", token)
      .then((res) => console.log(res));
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>Email</label>
      <input onChange={(e) => setEmail(e.target.value)} />
      <label>Password</label>
      <input onChange={(e) => setPassword(e.target.value)} />
      <button type="submit">Submit</button>
      <button onClick={handleLogout} type="button">
        Logout
      </button>
    </form>
  );
}

export default Home;
