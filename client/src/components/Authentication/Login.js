import AuthenticationLayout from "./AuthenticationLayout";
import {
  Input,
  SubmitButton,
  ButtonContainer,
  InputWrapper,
  Text,
  CancelButton,
} from "../styled-components";
import { Form, Formik } from "formik";
import styled from "styled-components";
import * as Yup from "yup";
import axios from "axios";
import { ENDPOINTS } from "../../constants";
import { useNavigate } from "react-router-dom";

const StyledForm = styled(Form)`
  height: 100%;
  display: grid;
  place-items: center;
`;

const validationSchema = Yup.object({
  email: Yup.string().required("Email is required.").email("Invalid Email"),
  password: Yup.string()
    .required("Password is required")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
    ),
});

function Login() {
  const navigate = useNavigate();

  const handleSubmit = async (values) => {
    const token = await axios.post(ENDPOINTS.LOG_IN, { ...values });
    localStorage.setItem("token", token.data);
    navigate("/admin");
  };

  return (
    <Formik
      initialValues={{ email: "", password: "" }}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
      validateOnChange={false}
    >
      {({ handleChange, ...props }) => (
        <StyledForm>
          <AuthenticationLayout
            width="450px"
            minHeight="auto"
            borderRadius="6px"
            pT="l"
            heading="Login"
          >
            <Text pH="xxl">Please enter your data to log in.</Text>

            <InputWrapper pT="l">
              <Input
                name="email"
                placeholder="Enter your email."
                label="Email"
                pH="xxl"
              />
              <Input
                name="password"
                pT="l"
                pH="xxl"
                placeholder="Enter your password."
                label="Password"
                type="password"
              />
            </InputWrapper>
            <ButtonContainer gap="20px" p="xxl">
              <CancelButton> Cancel</CancelButton>
              <SubmitButton type="submit">Submit</SubmitButton>
            </ButtonContainer>
          </AuthenticationLayout>
        </StyledForm>
      )}
    </Formik>
  );
}

export default Login;
