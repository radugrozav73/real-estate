import AuthenticationLayout from "./AuthenticationLayout";
import {
  Input,
  SubmitButton,
  ButtonContainer,
  InputWrapper,
  Text,
  CancelButton,
} from "../styled-components";
import { Form, Formik } from "formik";
import styled from "styled-components";
import * as Yup from "yup";
import axios from "axios";
import { ENDPOINTS } from "../../constants";

const StyledForm = styled(Form)`
  height: 100%;
  display: grid;
  place-items: center;
`;

const initialValues = {
  email: "",
  password: "",
  confirmation: "",
  firstName: "",
  lastName: "",
};

const validationSchema = Yup.object({
  email: Yup.string().required("Email is required.").email("Invalid Email"),
  password: Yup.string()
    .required("Password is required")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
    ),
  confirmation: Yup.string()
    .required("Confirmation is required.")
    .oneOf([Yup.ref("password")], "Confirmation password must match."),
  firstName: Yup.string().required("First name is required."),
  lastName: Yup.string().required("Last name is required."),
});

const handleSubmit = (values) => {
  axios
    .post(ENDPOINTS.REGISTER_USER, { ...values })
    .then((res) => console.log(res.data));
};

function Login() {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ handleChange }) => (
        <StyledForm>
          <AuthenticationLayout
            minWidth="450px"
            minHeight="400px"
            borderRadius="6px"
            pT="xxl"
            heading="Register"
          >
            <Text pH="xxl">Please complete next fields for registration.</Text>

            <InputWrapper pT="xxl">
              <Input
                name="email"
                placeholder="Enter your email"
                label="Email"
                pH="xxl"
                onChange={handleChange}
              />
              <Input
                name="firstName"
                pT="l"
                pH="xxl"
                placeholder="First name"
                label="First name"
              />
              <Input
                name="lastName"
                pT="l"
                pH="xxl"
                placeholder="Last name"
                label="Last name"
              />
              <Input
                name="password"
                pT="l"
                pH="xxl"
                placeholder="Enter your password"
                label="Password"
                type="password"
              />
              <Input
                name="confirmation"
                pT="l"
                pH="xxl"
                placeholder="Confirm your password."
                label="Confirmation password"
                type="password"
              />
            </InputWrapper>
            <ButtonContainer gap="20px" p="xxl">
              <CancelButton> Cancel</CancelButton>
              <SubmitButton type="submit">Submit</SubmitButton>
            </ButtonContainer>
          </AuthenticationLayout>
        </StyledForm>
      )}
    </Formik>
  );
}

export default Login;
