import React from "react";
import { Card, H2 } from "../styled-components";

const AuthenticationLayout = ({ children, heading, ...props }) => {
  return (
    <Card {...props}>
      <H2 m="xxl">{heading}</H2>
      {children}
    </Card>
  );
};

export default AuthenticationLayout;
