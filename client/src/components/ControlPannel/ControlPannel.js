import React from "react";
import Navbar from "./CPNavbar/Navbar";
import styled from "styled-components";
import DashBoard from "./Dashboard/Dashboard";

const Wrapper = styled.div`
  height: 100%;
  display: flex;
`;

function ControlPannel() {
  return (
    <Wrapper>
      <Navbar />
      <DashBoard />
    </Wrapper>
  );
}

export default ControlPannel;
