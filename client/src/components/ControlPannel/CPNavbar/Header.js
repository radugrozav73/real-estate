import styled from "styled-components";
import { H2 } from "../../styled-components";
import propTypes from "prop-types";

const PropTypes = {
  // Text that describes the section that the user is currently in
  section: propTypes.string,
};

const InfoHeader = styled.div`
  width: 100%;
  position: absolute;
  top: 0;
  display: flex;
  align-items: center;
  height: 60px;
  background-color: ${(props) => props.theme.colors.infoHeader};
  box-shadow: ${(props) => props.theme.boxShadow.card};
  clip-path: -25px 0 -5px 25px;
  margin: 0;
  color: ${(props) => props.theme.colors.infoHeaderColor};
`;

function Header({ section }) {
  return (
    <InfoHeader>
      <H2 mL="xxl">{section}</H2>
    </InfoHeader>
  );
}

Header.propTypes = PropTypes;
export default Header;
