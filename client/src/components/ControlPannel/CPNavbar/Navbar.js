import React, { useState } from "react";
import styled from "styled-components";
import { Arrow, Link, Flex, H2, H3 } from "../../styled-components";

const NavbarLayout = styled(Flex)`
  height: 100%;
  width: ${({ isOpen }) => (isOpen ? "250px" : "50px")};
  border-right: 1px solid ${(props) => props.theme.colors.border};
  box-shadow: ${(props) => props.theme.boxShadow.card};
  background-color: ${(props) => props.theme.colors.navbarBlue};
  color: ${(props) => props.theme.colors.whiteText};
  transition: 0.3s ease-in-out;
`;

function Navbar() {
  const [isOpen, setIsOpen] = useState(false);

  const handleLogOut = () => {
    // logs admin out, TODO: implement functionality
    console.log("gonna log out");
  };

  const toggleMenu = () => {
    // console.log("toggled");
    setIsOpen(!isOpen);
  };

  return (
    <NavbarLayout
      direction="column"
      justifyContent="space-between"
      pL={isOpen ? "xxl" : "none"}
      isOpen={isOpen}
    >
      <Flex
        top="20px"
        direction={!isOpen ? "column" : "row"}
        justifyContent={isOpen ? "space-between" : "center"}
      >
        {isOpen ? (
          <H3>RealEstate</H3>
        ) : (
          <H3>
            R<br />E
          </H3>
        )}
        <Flex
          mT={isOpen ? "none" : "l"}
          justifyContent={isOpen ? "flex-end" : "center"}
          mR={isOpen ? "l" : "none"}
        >
          <Arrow onClick={toggleMenu} rotateArrow={!isOpen} />
        </Flex>
      </Flex>
      <Flex bottom="150px" justifyContent="flex-start">
        {isOpen && <Link to="/admin/home">Home</Link>}
      </Flex>
      <Flex
        textAlign="center"
        bottom="40px"
        justifyContent={isOpen ? "flex-start" : "center"}
      >
        <Link to="#" onClick={handleLogOut}>
          Log {!isOpen && <br />}
          out
        </Link>
      </Flex>
    </NavbarLayout>
  );
}

export default Navbar;
