import React from "react";
import { Flex } from "../../styled-components";
import Header from "../CPNavbar/Header";
import DashboardCards from "./DashboardCards";
import LatestListings from "./LatestListings";

const whiteList = [
  {
    cardFor: "Agents",
    number: 4,
    cardDetails: "See the agents that operates on the website.",
  },
  {
    cardFor: "Agents",
    number: 4,
    cardDetails: "See the agents that operates on the website.",
  },
  {
    cardFor: "Agents",
    number: 4,
    cardDetails: "See the agents that operates on the website.",
  },
  {
    cardFor: "Agents",
    number: 4,
    cardDetails: "See the agents that operates on the website.",
  },
];

function DashBoard() {
  return (
    <Flex direction="column" p="l">
      <Header section="Dashboard" />
      <DashboardCards cardDescriptors={whiteList} />
      <LatestListings />
    </Flex>
  );
}
export default DashBoard;
