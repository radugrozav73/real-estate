import styled from "styled-components";
import { Flex, H1, Text } from "../../styled-components";
import propTypes from "prop-types";

const PropTypes = {
  // this prop describes the card
  cardDescriptors: propTypes.array.isRequired,
};

const Card = styled.div`
  height: 200px;
  width: 400px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border: 1px solid ${(props) => props.theme.colors.border};
  border-radius: ${(props) => props.theme.radius.m};
  box-shadow: ${(props) => props.theme.boxShadow.card};
  cursor: pointer;
  transition: 0.3s;
  background-color: ${(props) => props.theme.colors.cardBackground};
  color: ${(props) => props.theme.colors.cardText};

  :hover {
    box-shadow: ${(props) => props.theme.boxShadow.cardHover};
    transition: 1s;
  }
`;

function DashboardCards({ cardDescriptors = [] }) {
  return (
    <Flex top="50px" gap="40px">
      {cardDescriptors.map((el, i) => {
        return (
          <Card>
            <H1 mL="l">{el.cardFor}</H1>
            <Flex
              mR="l"
              height="100%"
              alignItems="flex-start"
              justifyContent="space-evenly"
              direction="column"
              maxWidth="150px"
            >
              <Text>{el.cardDetails}</Text>
              {el.number && <Text>Number: {el.number}</Text>}
            </Flex>
          </Card>
        );
      })}
    </Flex>
  );
}

DashboardCards.propTypes = PropTypes;
export default DashboardCards;
