import styled from "styled-components";
import propTypes from "prop-types";
import { Card, Flex } from "../../styled-components";

const CarouselWrapper = styled.div`
  margin-bottom: 100px;
`;

function LatestListings() {
  return (
    <CarouselWrapper>
      <Flex top="50px" bottom="100px" p="xxl" justifyContent="flex-start">
        <Card width="200px" height="100px"></Card>
      </Flex>
    </CarouselWrapper>
  );
}

export default LatestListings;
