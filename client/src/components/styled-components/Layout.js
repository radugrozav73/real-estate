import styled from "styled-components";
import propTypes from "prop-types";

const PropTypes = {
  // specifies the overflow type
  overflowX: propTypes.string,
  overflowY: propTypes.string,
};

export const LayoutCentered = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: "center";
  align-items: "center";
  flex-direction: "column";
  overflow-y: ${(props) => props.overflowY || "auto"};
  overflow-x: ${(props) => props.overflowX || "auto"};
`;

export const LayoutSimple = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  /* justify-content: "flex-start";
  align-items: "flex-start"; */
  flex-direction: "column";
  overflow-y: ${(props) => props.overflowY || "auto"};
  overflow-x: ${(props) => props.overflowX || "auto"};
`;

LayoutSimple.propTypes = PropTypes;
LayoutCentered.propTypes = PropTypes;
