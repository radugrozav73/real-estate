import styled from "styled-components";
import { Flex } from ".";

export const ButtonContainer = styled(Flex)`
  transform: ${(props) => props.position === "absolute" && "translateY(-100%)"};
`;
