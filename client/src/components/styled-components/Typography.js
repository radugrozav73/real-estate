import styled from "styled-components";
import { Marginator } from "./Marginator";
import { Link as ReactLink } from "react-router-dom";
import propTypes from "prop-types";

const StyledH1 = styled.h1`
  font-size: ${(props) => props.fontSize};
  color: ${(props) => props.theme.colors[props.color]};
  font-weight: ${(props) =>
    props.theme.weight[props.weight] || props.theme.weight.normal};
  text-align: ${(props) => props.textAlign};
`;

const StyledH2 = styled.h2`
  font-size: ${(props) => props.fontSize};
  color: ${(props) => props.theme.colors[props.color]};
  font-weight: ${(props) =>
    props.theme.weight[props.weight] || props.theme.weight.normal};
  text-align: ${(props) => props.textAlign};
`;

const StyledH3 = styled.h3`
  font-size: ${(props) => props.fontSize};
  color: ${(props) => props.theme.colors[props.color]};
  text-align: ${(props) => props.textAlign};
`;

const StyledText = styled.p`
  font-size: ${(props) => props.fontSize};
  color: ${(props) => props.theme.colors[props.color]};
  font-size: ${(props) =>
    props.theme.fontSize[props.fontSize] || props.theme.fontSize.text};
  text-align: ${(props) => props.textAlign};
`;

const StyledLabel = styled.label`
  font-size: ${(props) => props.fontSize};
  color: ${(props) =>
    props.theme.colors[props.color] || props.theme.colors.label};
  font-size: 12px;
  text-align: ${(props) => props.textAlign};
`;

const StyledLink = styled(ReactLink)`
  color: ${(props) =>
    props.theme.colors[props.color] || props.theme.colors.whiteText};
  text-decoration: none;
  text-align: ${(props) => props.textAlign};
  transition: 0.3s ease-in-out;
  font-size: ${(props) =>
    props.theme.fontSize[props.fontSize] || props.theme.fontSize.link};

  :hover {
    color: ${({ theme }) => theme.colors.hover};
  }
`;

export const H1 = ({ children, weight, ...props }) => {
  return (
    <Marginator {...props}>
      <StyledH1 weight={weight} fontSize="32px">
        {children}
      </StyledH1>
    </Marginator>
  );
};

export const H2 = ({ children, weight, ...props }) => {
  return (
    <Marginator {...props}>
      <StyledH2 weight={weight} fontSize="24px">
        {children}
      </StyledH2>
    </Marginator>
  );
};

export const H3 = ({ children, weight, ...props }) => {
  return (
    <Marginator {...props}>
      <StyledH3 weight={weight} fontSize="18px">
        {children}
      </StyledH3>
    </Marginator>
  );
};

export const Text = ({ children, color, fontSize, ...props }) => {
  return (
    <Marginator {...props}>
      <StyledText color={color} fontSize={fontSize}>
        {children}
      </StyledText>
    </Marginator>
  );
};

export const Label = ({ children, color, htmlFor, ...props }) => {
  return (
    <Marginator {...props}>
      <StyledLabel color={color} htmlFor={htmlFor}>
        {children}
      </StyledLabel>
    </Marginator>
  );
};

export const Link = ({
  children,
  to,
  fontSize,
  color,
  textAlign,
  htmlFor,
  ...props
}) => {
  return (
    <Marginator {...props}>
      <StyledLink
        fontSize={fontSize}
        textAlign={textAlign}
        to={to}
        color={color}
      >
        {children}
      </StyledLink>
    </Marginator>
  );
};

const LabelPropTypes = {
  // this prop represents the htmlFor attribute
  htmlFor: propTypes.string,
};

const UniversalPropTypes = {
  // prop that specifies the font-size
  fontSize: propTypes.string,
  // prop that specifies the font-weight
  weight: propTypes.string,
  // prop that specifies the color
  color: propTypes.string,
  // prop that specifies how the text should be aligned
  textAlign: propTypes.string,
};

Link.propTypes = { ...UniversalPropTypes };
Label.propTypes = { ...UniversalPropTypes, ...LabelPropTypes };
Text.propTypes = { ...UniversalPropTypes };
H3.propTypes = { ...UniversalPropTypes };
H2.propTypes = { ...UniversalPropTypes };
H1.propTypes = { ...UniversalPropTypes };
