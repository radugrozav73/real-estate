import styled from "styled-components";
import propTypes from "prop-types";

const PropTypes = {
  // specifies if the arrow should rotate 180 deg
  rotateArrow: propTypes.bool,
};

const StyledArrow = styled.div`
  transform: ${(props) =>
    props.rotateArrow ? "rotate(270deg)" : "rotate(90deg)"};
  transition: 0.3s;
  cursor: pointer;

  .arrow {
    width: 30px;
    height: 30px;
    border: 2px solid #fff;
    border-radius: 50%;
    position: relative;
    transition: ease-in;
  }
  .arrow::before {
    content: "";
    position: absolute;
    width: 10px;
    height: 10px;
    top: 50%;
    left: 50%;
    border-left: 2px solid #fafafa;
    border-bottom: 2px solid #fafafa;
    transform: translate(-50%, -65%) rotate(-45deg);
  }
`;

function Arrow({ rotateArrow, onClick }) {
  return (
    <StyledArrow onClick={onClick} rotateArrow={rotateArrow}>
      <div className="arrow"></div>
    </StyledArrow>
  );
}

Arrow.propTypes = PropTypes;

export default Arrow;
