import styled from "styled-components";
import propTypes from "prop-types";

const PropTypes = {
  height: propTypes.string,
  width: propTypes.string,
};

export const SubmitButton = styled.button`
  height: ${(props) => props.height || "40px"};
  width: ${(props) => props.width || "120px"};
  outline: none;
  cursor: pointer;
  border: none;
  border-radius: 6px;
  transition: 0.2s ease-in-out;
  color: #ffff;
  background-color: ${(props) => props.theme.colors.submit};

  :hover {
    background-color: ${(props) => props.theme.colors.submitHover};
    transition: 0.2s ease-in-out;
  }
`;

export const CancelButton = styled(SubmitButton)`
  background-color: ${(props) => props.theme.colors.cancel};

  :hover {
    background-color: ${(props) => props.theme.colors.cancelHover};
  }
`;

SubmitButton.propTypes = PropTypes;
