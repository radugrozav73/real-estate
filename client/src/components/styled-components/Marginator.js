import styled from "styled-components";
import propTypes from "prop-types";

const PropTypes = {
  // This props indicate the margin or padding V-vertical, H-horizontal, T,B,R,L = [top, bottom, right, left]
  m: propTypes.string,
  mV: propTypes.string,
  mR: propTypes.string,
  mL: propTypes.string,
  mT: propTypes.string,
  mB: propTypes.string,
  p: propTypes.string,
  pV: propTypes.string,
  pR: propTypes.string,
  pL: propTypes.string,
  pT: propTypes.string,
  pB: propTypes.string,
};

export const Marginator = styled.div`
  margin-top: ${(props) =>
    props.theme.margin[props.mT] || props.theme.margin[props.mV]};
  margin-bottom: ${(props) =>
    props.theme.margin[props.mB] || props.theme.margin[props.mV]};
  margin-left: ${(props) =>
    props.theme.margin[props.mL] || props.theme.margin[props.mH]};
  margin-right: ${(props) =>
    props.theme.margin[props.mR] || props.theme.margin[props.mH]};
  margin: ${(props) => props.theme.margin[props.m]};

  padding-top: ${(props) =>
    props.theme.padding[props.pT] || props.theme.padding[props.pV]};
  padding-bottom: ${(props) =>
    props.theme.padding[props.pB] || props.theme.padding[props.pV]};
  padding-left: ${(props) =>
    props.theme.padding[props.pL] || props.theme.padding[props.pH]};
  padding-right: ${(props) =>
    props.theme.padding[props.pR] || props.theme.padding[props.pH]};
  padding: ${(props) => props.theme.padding[props.p]};
`;

Marginator.propTypes = PropTypes;
