import { Flex } from ".";
import styled from "styled-components";

export const InputWrapper = styled(Flex)`
  justify-content: ${(props) => props.justifyContent || "flex-start"};
  flex-direction: ${(props) => props.direction || "column"};
  align-items: ${(props) => props.alignItems || "flex-start"};
`;
