import styled from "styled-components";
import propTypes from "prop-types";

const PropTypes = {
  width: propTypes.string,
  minWidth: propTypes.string,
  height: propTypes.string,
  minHeight: propTypes.string,
  borderRadius: propTypes.string,
  position: propTypes.string,
  top: propTypes.string,
  bottom: propTypes.string,
  left: propTypes.string,
  right: propTypes.string,
  maxWidth: propTypes.string,
  maxHeight: propTypes.string,
};

export const Card = styled.div`
  width: ${(props) => props.width};
  min-width: ${(props) => props.minWidth};
  min-height: ${(props) => props.minHeight};
  max-width: ${(props) => props.maxWidth};
  max-height: ${(props) => props.maxHeight};
  height: ${(props) => props.height};
  box-shadow: ${(props) => props.theme.boxShadow.card};
  border: 1px solid ${(props) => props.theme.colors.border};
  border-radius: ${(props) => props.borderRadius};
  position: ${(props) => props.position || "relative"};
  top: ${(props) => props.top};
  bottom: ${(props) => props.bottom};
  left: ${(props) => props.left};
  right: ${(props) => props.right};
`;

Card.propTypes = PropTypes;
