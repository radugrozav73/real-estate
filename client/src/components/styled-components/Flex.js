import styled from "styled-components";
import { Marginator } from ".";
import propTypes from "prop-types";

const PropTypes = {
  // justify-content
  justifyContent: propTypes.string,
  // align-items
  alignItems: propTypes.string,
  // flex-direction
  direction: propTypes.string,
  // gap,
  gap: propTypes.string,
  width: propTypes.string,
  height: propTypes.string,
  position: propTypes.string,
  top: propTypes.string,
  bottom: propTypes.string,
  left: propTypes.string,
  right: propTypes.string,
  maxHeight: propTypes.string,
  maxWidth: propTypes.string,
  // text-align
  textAlign: propTypes.string,
  wrap: propTypes.string,
};

export const Flex = styled(Marginator)`
  display: flex;
  justify-content: ${(props) => props.justifyContent || "center"};
  align-items: ${(props) => props.alignItems || "center"};
  flex-direction: ${(props) => props.direction || "row"};
  gap: ${(props) => props.gap};
  width: ${(props) => props.width || "100%"};
  height: ${(props) => props.height || "auto"};
  position: ${(props) => props.position || "relative"};
  top: ${(props) => props.top};
  bottom: ${(props) => props.bottom};
  left: ${(props) => props.left};
  flex-wrap: ${(props) => props.wrap || "wrap"};
  right: ${(props) => props.right};
  text-align: ${(props) => props.textAlign};
  max-width: ${(props) => props.maxWidth};
  max-height: ${(props) => props.maxHeight};
`;

Flex.propTypes = PropTypes;
