import { Label, Flex } from "./index";
import styled from "styled-components";
import { useField } from "formik";
import propTypes from "prop-types";

const PropTypes = {
  // input type
  type: propTypes.string,
  // input placeholder
  placeholder: propTypes.string,
};

const StyledInput = styled.input`
  width: 100%;
  border: none;
  border-bottom: 1px solid
    ${(props) =>
      props.error ? props.theme.colors.error : props.theme.colors.input};
  padding: 2px 0;
  padding-left: 5px;
  outline: none;
  caret-color: ${(props) => props.theme.colors.border};
  color: ${(props) => props.theme.colors.label};

  ::placeholder {
    color: ${(props) => props.theme.colors.placeholder};
    font-size: ${(props) => props.theme.fontSize.placeholder};
  }
`;

export const Input = ({ label, type, placeholder, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <Flex
      gap="5px"
      width="100%"
      {...props}
      direction="column"
      alignItems="flex-start"
    >
      {label && <Label color={meta.error && "error"}>{label}</Label>}
      <StyledInput
        error={!!meta.error}
        type={type}
        placeholder={placeholder}
        {...field}
      />
      {meta.error && meta.touched ? (
        <Label color="error">{meta.error}</Label>
      ) : null}
    </Flex>
  );
};
