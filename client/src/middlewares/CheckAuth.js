import React, { useContext, useEffect, useState } from "react";
import Loader from "../components/Loader";
import styled from "styled-components";
import axios from "axios";
import { ENDPOINTS } from "../constants";
import { Navigate } from "react-router";
import { AuthContext } from "../components/context";

const LoaderModal = styled.div`
  height: 100%;
  position: absolute;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

function CheckAuth({ children }) {
  const value = useContext(AuthContext);
  const [status, setStatus] = useState(
    value.isAuthenticated ? "success" : "loading"
  );
  const token = localStorage.getItem("token");

  console.log(status, value.isAuthenticated);

  useEffect(() => {
    !token ||
      (!value.isAuthenticated &&
        axios
          .get(`${ENDPOINTS.CHECK_USER}`, {
            headers: {
              Authorization: `${token}`,
            },
          })
          .then((res) => {
            value.setAuthentication();
            console.log("salut");
            setStatus("success");
          })
          .catch((er) => {
            setStatus("error");
          }));
  }, []);

  if (!token) {
    return <Navigate to="/login" />;
  }

  return (
    <>
      {status === "loading" && (
        <LoaderModal>
          <Loader color="blue" />
        </LoaderModal>
      )}
      {status === "success" && <>{children}</>}
    </>
  );
}

export default CheckAuth;
