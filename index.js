const express = require("express");
const homeRoutes = require("./server/routes/homeRoutes");
const userRoutes = require("./server/routes/userRoutes");
const cors = require("cors");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");

dotenv.config({ path: "./.env" });
const DB = process.env.DATABASE;
mongoose.connect(DB);

const corsOptions = {
  origin: "http://localhost:3001",
  optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(homeRoutes);
app.use(userRoutes);

app.listen("3000");
